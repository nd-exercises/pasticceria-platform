-- CREATE TABLES
CREATE TABLE user (
  id VARCHAR(255) NOT NULL DEFAULT (uuid()),
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  active BOOLEAN NOT NULL DEFAULT true,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  surname VARCHAR(255) NOT NULL,
  last_login DATETIME,
  PRIMARY KEY (id)
) CHARACTER SET utf8;
CREATE UNIQUE INDEX un_user_email ON user(email);

CREATE TABLE recipe (
  id VARCHAR(255) NOT NULL DEFAULT (uuid()),
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  active BOOLEAN NOT NULL DEFAULT true,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  base_price NUMERIC(15, 2) NOT NULL,
  PRIMARY KEY (id)
) CHARACTER SET utf8;
CREATE UNIQUE INDEX un_recipe_name ON recipe(name);

CREATE TABLE ingredient (
  id VARCHAR(255) NOT NULL DEFAULT (uuid()),
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  active BOOLEAN NOT NULL DEFAULT true,
  recipe_id VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  quantity NUMERIC(15, 2) NOT NULL,
  uom VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (recipe_id) REFERENCES recipe(id)
) CHARACTER SET utf8;
CREATE UNIQUE INDEX un_recipe_ingredient ON ingredient(recipe_id, name);

CREATE TABLE cake (
  id VARCHAR(255) NOT NULL DEFAULT (uuid()),
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  active BOOLEAN NOT NULL DEFAULT true,
  recipe_id VARCHAR(255) NOT NULL,
  production_date DATE NOT NULL,
  sale_date DATETIME,
  PRIMARY KEY (id),
  FOREIGN KEY (recipe_id) REFERENCES recipe(id)
) CHARACTER SET utf8;

-- CREATE VIEWS
CREATE OR REPLACE VIEW v_cacke_shop AS
SELECT
  r.id AS recipe_id, r.name AS recipe_name, r.base_price, c.id AS cake_id, c.production_date, r.base_price AS price
FROM
  recipe r
  INNER JOIN cake c ON r.id = c.recipe_id
WHERE
  r.active = true
  AND c.active = true
  AND c.sale_date IS NULL
  AND DATEDIFF(now(), c.production_date) = 0
UNION ALL
SELECT
  r.id AS recipe_id, r.name AS recipe_name, r.base_price, c.id AS cake_id, c.production_date, r.base_price * 0.8 AS price
FROM
  recipe r
  INNER JOIN cake c ON r.id = c.recipe_id
WHERE
  r.active = true
  AND c.active = true
  AND c.sale_date IS NULL
  AND DATEDIFF(now(), c.production_date) = 1
UNION ALL
SELECT
  r.id AS recipe_id, r.name AS recipe_name, r.base_price, c.id AS cake_id, c.production_date, r.base_price * 0.2 AS price
FROM
  recipe r
  INNER JOIN cake c ON r.id = c.recipe_id
WHERE
  r.active = true
  AND c.active = true
  AND c.sale_date IS NULL
  AND DATEDIFF(now(), c.production_date) = 2;
