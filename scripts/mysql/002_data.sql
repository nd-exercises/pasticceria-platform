-- INSERT DATA
INSERT INTO 
    user (email, password, name, surname)
VALUES
    ('luana@interlogica.it', MD5('luana'), 'Luana', 'Interlogica'),
    ('maria@interlogica.it', MD5('maria'), 'Maria', 'Interlogica');

INSERT INTO 
    recipe (name, base_price, description) 
VALUES 
    ('ricetta 1', 100, 'testo della ricetta uno'), 
    ('ricetta 2', 75), NULL;

INSERT INTO
    ingredient (recipe_id, name, quantity, uom)
VALUES
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), 'farina 00', 300, 'g'),
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), 'acqua', 200, 'ml'),
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), 'sale', 1, 'qb'),
    ((SELECT id FROM recipe WHERE name = 'ricetta 2'), 'olio', 5, 'g'),
    ((SELECT id FROM recipe WHERE name = 'ricetta 2'), 'uovo', 1, 'medie');

INSERT INTO 
    cake (recipe_id, production_date) 
VALUES
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), subdate(current_date, 0)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), subdate(current_date, 0)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), subdate(current_date, 1)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), subdate(current_date, 2)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 1'), subdate(current_date, 3)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 2'), subdate(current_date, 0)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 2'), subdate(current_date, 2)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 2'), subdate(current_date, 30)),
    ((SELECT id FROM recipe WHERE name = 'ricetta 2'), subdate(current_date, -10));
