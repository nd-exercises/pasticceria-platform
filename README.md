# Pasticceria

## Esercizio

La pasticceria vende dolci che hanno un nome ed un prezzo. Ogni dolce è composto da una lista di ingredienti. Opzionale: indicare di ogni ingrediente quantità e unità di misura.

Opzionale: La gestione della pasticceria è in mano a Luana e Maria che vogliono avere il proprio account per poter accedere all'area di backoffice tramite email e password.

Nell’area di backoffice si possono gestire (CRUD) i dolci e metterli in vendita con una certa disponibilità (esempio: 3 torte paradiso in vendita). I dolci in vendita invecchiano ed in base al tempo trascorso dalla loro messa in vendita hanno prezzi diversi: primo giorno prezzo pieno, secondo giorno costano l’80%, il terzo giorno il 20%. Il quarto giorno non sono commestibili e devono essere ritirati dalla vendita.

Realizzare una pagina vetrina dove tutti possono vedere la lista di dolci disponibili e il prezzo relativo.

Opzionale: andando nella pagina del dettaglio del dolce (o tramite overlayer), si scoprono gli ingredienti indicati dalla ricetta.

## Architettura

### Platform

Contiene tutti i file che servono a livello di piattaforma, compresi i file docker compose e gli script sql

### Service

Espone le API Rest di backend

### Web

Presenta l'interfaccia web del progetto

## Avvio progetto

### Prerequisiti

- Docker e Docker Compose.

  - Gli utenti Windows e Mac installano automaticamente Compose con Docker per Windows / Mac.

  - Gli utenti Linux possono leggere [istruzioni per l'installazione](https://docs.docker.com/compose/install/#install-compose) o possono installare tramite pip: `pip install docker-compose`

### Script di avvio

    docker-compose up

### Se non si vuole usare docker

- Creare un database MySql

- Aggiornare il file `.env` con i dati corretti

- Settare le vairiabili d'ambiente contenute nel file `.env`

- Nella cartella `serivices`

  - eseguire il comando `npm install`

  - eseguire il comando `npm run build-ts`

  - eseguire il comando `npm start`

- Nella cartella `webapp`

  - eseguire il comando `npm install`

  - eseguire il comando `npm run build-ts`

- Installare un webserver e configurarlo per accedere ai file statici contenuti nella cartella `webapp/dist` e fare da reverse proxy verso i servizi precedentemente avviati; se il webserver è **nginx** è possibile utilizzare il file di configurazione presente nella cartella `scripts/nginx/`, adeguatamente aggiustato

## Dati di test

- **Luana**
  - username: `luana@interlogica.it`
  - password: `luana`

- **Maria**
  - username: `maria@interlogica.it`
  - password: `maria`

## Author

- Nico Dante <[info@nicodante.it](mailto:info@nicodante.it)>
